import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { GccProjectSharedLibsModule, GccProjectSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';

@NgModule({
  imports: [GccProjectSharedLibsModule, GccProjectSharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [JhiLoginModalComponent],
  exports: [GccProjectSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GccProjectSharedModule {
  static forRoot() {
    return {
      ngModule: GccProjectSharedModule
    };
  }
}
