/**
 * View Models used by Spring MVC REST controllers.
 */
package com.fpuna.py.web.rest.vm;
